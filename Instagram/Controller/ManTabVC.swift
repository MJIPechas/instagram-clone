//
//  ManTabVC.swift
//  Instagram
//
//  Created by Mohammad Jahir on 10/28/19.
//  Copyright © 2019 Mohammad Jahir. All rights reserved.
//

import UIKit
import Firebase

class ManTabVC: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        configureViewControllers()
        
        //User validation
        checkUserIfLoggedIn()
    }
    
    //Function to create view Controller that exist within tab bar Contoller
    
    func configureViewControllers(){
        
        //home feed controller
       let feedVC =  constructNavController(unselectedImage: #imageLiteral(resourceName: "home_unselected"), selectedImage: #imageLiteral(resourceName: "home_selected"), rootViewController: FeedVC(collectionViewLayout: UICollectionViewFlowLayout()))
        
        //Search feed controller
        let searchVC = constructNavController(unselectedImage: #imageLiteral(resourceName: "search_unselected"), selectedImage: #imageLiteral(resourceName: "search_selected"), rootViewController: SearchVC())
        
        //post controller
         let postVC = constructNavController(unselectedImage: #imageLiteral(resourceName: "plus_unselected"), selectedImage: #imageLiteral(resourceName: "plus_unselected"), rootViewController: UploadPostVC())
        
        //notification controller
        let notificationVC = constructNavController(unselectedImage: #imageLiteral(resourceName: "like_unselected"), selectedImage: #imageLiteral(resourceName: "like_selected"), rootViewController: NotificationVC())
        
        //profile controller
        let profileVC = constructNavController(unselectedImage: #imageLiteral(resourceName: "profile_unselected"), selectedImage: #imageLiteral(resourceName: "profile_selected"), rootViewController: UserPofileVC(collectionViewLayout: UICollectionViewFlowLayout()))
        
        viewControllers = [feedVC,searchVC,postVC,notificationVC,profileVC]
        tabBar.tintColor = .black
    }
    
   
//    // Construct Navigation controller
    func constructNavController(unselectedImage: UIImage, selectedImage: UIImage,
    rootViewController: UIViewController = UIViewController())->UINavigationController{
        
        //Construct nav controller
        let navController = UINavigationController(rootViewController: rootViewController)
        navController.tabBarItem.image = unselectedImage
        navController.tabBarItem.selectedImage = selectedImage
        navController.navigationBar.tintColor = .black
        
        return navController
        
    }
    
    func checkUserIfLoggedIn(){
        
        if Auth.auth().currentUser == nil{
            DispatchQueue.main.async {
                print("User not logged in ")
                  let loginVC = LoginVC()
                  let navControllerVC = UINavigationController(rootViewController: loginVC)
                  navControllerVC.modalPresentationStyle = .fullScreen
                  self.present(navControllerVC, animated: true, completion: nil)
            }
  
            //CPL.cPrint("User not logged in")
            
        }
    }
}
