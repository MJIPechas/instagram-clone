//
//  SignUpVC.swift
//  Instagram
//
//  Created by Mohammad Jahir on 10/26/19.
//  Copyright © 2019 Mohammad Jahir. All rights reserved.
//

import UIKit
import Firebase

class SignUpVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var imageSelected = false
    
    let plusPhotoButton : UIButton = {
       
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "plus_photo").withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(handleSelectProfilePhoto), for: .touchUpInside)
        return button
    }()
    
    let emailTextField : UITextField = {
       
        let tf = UITextField()
        tf.placeholder = "Email"
        tf.borderStyle = .roundedRect
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.font = UIFont.systemFont(ofSize: 14)
        tf.textColor = UIColor.black
        tf.addTarget(self, action: #selector(formValidation), for: .editingChanged)
        return tf;
    }()
    let passwordTextField : UITextField = {
       
        let ptf = UITextField()
        ptf.placeholder = "Password"
        ptf.isSecureTextEntry = true
        ptf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        ptf.borderStyle = .roundedRect
        ptf.font = UIFont.systemFont(ofSize: 14)
        ptf.textColor = UIColor.black
        ptf.addTarget(self, action: #selector(formValidation), for: .editingChanged)
        return ptf
    }()
    let fullNameTextField : UITextField = {
       
        let fntf = UITextField()
        fntf.placeholder = "Full Name"
        fntf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        fntf.borderStyle = .roundedRect
        fntf.font = UIFont.systemFont(ofSize: 14)
        fntf.textColor = UIColor.black
        fntf.addTarget(self, action: #selector(formValidation), for: .editingChanged)

        return fntf
    }()
    let userNameTextField : UITextField = {
       
        let untf = UITextField()
        //untf.attributedPlaceholder = NSAttributedString(string: "User Name", attributes: [NSAttributedString.Key.foregroundColor : UIColor(white: 1, alpha: 0.5)])
        untf.placeholder = "User Name"
        untf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        untf.borderStyle = .roundedRect
        untf.font = UIFont.systemFont(ofSize: 14)
        untf.textColor = UIColor.black
        untf.addTarget(self, action: #selector(formValidation), for: .editingChanged)

        return untf
    }()
    let signUpButton : UIButton = {
       
        let button = UIButton(type: .system)
        button.setTitle("Sign Up", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor(red: 149/255, green: 204/255, blue: 244/255, alpha: 1)
        button.layer.cornerRadius = 5;
        button.isEnabled = false
        button.addTarget(self, action: #selector(handleSignUp), for: .touchUpInside)
        
        return button
    }()
    let alreadyHaveAnAccountButton : UIButton = {
       
        let button = UIButton(type: .system)
        let attributedTitle = NSMutableAttributedString(string: "Already have an account?  ", attributes:
            [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor:UIColor.lightGray ])
        attributedTitle.append(NSAttributedString(string: "Sign in", attributes: [NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: UIColor(displayP3Red: 17/255, green: 154/255, blue: 237/255, alpha: 1)]))
        
       
               
        
        
        button.setAttributedTitle(attributedTitle, for: .normal)
        button.addTarget(self, action: #selector(handleShowSignIn), for: .touchUpInside)
        
        return button
    }()
    
    //Function Where everything loading
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(plusPhotoButton)
        plusPhotoButton.anchor(top: view.topAnchor, left: nil, bottom: nil, right: nil, paddingTop: 40, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 140, height: 140)
        plusPhotoButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        view.addSubview(alreadyHaveAnAccountButton)
        alreadyHaveAnAccountButton.anchor(top: nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        
        configureViewComponent()
        
    }
    //Configure Stack view for email fullName password field
    func configureViewComponent(){
        
        let stackView = UIStackView(arrangedSubviews: [emailTextField,fullNameTextField,userNameTextField,passwordTextField,signUpButton])
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.distribution = .fillEqually
        
        view.addSubview(stackView)
        
        stackView.anchor(top: plusPhotoButton.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 24, paddingLeft: 40, paddingBottom:0, paddingRight: 40, width: 0, height: 240)
        
        
    }
    //After picked image from gallery set it on the app screen in a circle
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let profileImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            imageSelected = false
            return
        }
        
        // User select a profile image
        imageSelected = true
        
        plusPhotoButton.layer.cornerRadius = plusPhotoButton.frame.width/2
        plusPhotoButton.layer.masksToBounds = true
        plusPhotoButton.layer.borderColor = UIColor.black.cgColor
        plusPhotoButton.layer.borderWidth = 1
        plusPhotoButton.setImage(profileImage.withRenderingMode(.alwaysOriginal), for: .normal)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: - Button Target function Implementation
    @objc func handleShowSignIn(){
       
        _ = navigationController?.popViewController(animated: true)
    }
    @objc func handleSignUp(){
       
        guard let email = emailTextField.text else{return}
        guard let password = passwordTextField.text else{return}
        guard let fullName = fullNameTextField.text else{return}
        guard let userName = userNameTextField.text else {return}
        
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if let error = error{
                print("Something is wrong \(error.localizedDescription)")
                return
            }
           
            //Set Profile image
            guard let profileImage = self.plusPhotoButton.imageView?.image else {return}
            //upload data
            guard let uploadData = profileImage.jpegData(compressionQuality: 0.3) else {return}
            //Place Image in firebase storage
            let fileName = NSUUID().uuidString
            let imageReference = Storage.storage().reference().child("profile_image").child(fileName)
            imageReference.putData(uploadData, metadata: nil) { (metadata, error) in
                
                if let error = error {
                    print("Failed to upload image in firebase storage with error \(error.localizedDescription)")
                }
                
                imageReference.downloadURL { (url, error) in
                    if let error = error{
                        print("Something is wrong getting profile url \(error)")
                    }
                    
                    //Profile Image URL
                    guard let profileImageURL = url?.absoluteString else {return}
                   // guard let profileImageURL = (metadata?.downloadURL() as AnyObject).absoluteString else { return }
                    let dictionaryValues = ["name": fullName,
                                            "userName": userName,
                                            "profileImageURL": profileImageURL ] as [String : Any]
                    //User ID
                    guard let uid = user?.user.uid else { return }
                    let values = [uid : dictionaryValues]
                    Database.database().reference().child("users").updateChildValues(values) { (error, ref) in
                        print("Successfully created user and saved information into database ")
                    }
                    //Save our information to database
                }
                
            }
            print("SignUP Successful")
        }
    }
    @objc func formValidation(){
        guard emailTextField.hasText,
              passwordTextField.hasText,
              fullNameTextField.hasText,
              userNameTextField.hasText,
              imageSelected == true else{
              
          
                signUpButton.isEnabled = false
                signUpButton.backgroundColor = UIColor(red: 149/255, green: 204/255, blue: 244/255, alpha: 1)

                return
        }
        signUpButton.isEnabled = true
        signUpButton.backgroundColor = UIColor(red: 17/255, green: 154/255, blue: 237/255, alpha: 1)
            
    }
    //Pick Image from user gallery
    @objc func handleSelectProfilePhoto(){
        
        //Configure Image Picker
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        //Present Image Picler
        self.present(imagePicker, animated: true, completion: nil)
    }
}

