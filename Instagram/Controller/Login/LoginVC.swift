//
//  LoginVC.swift
//  Instagram
//
//  Created by Mohammad Jahir on 10/24/19.
//  Copyright © 2019 Mohammad Jahir. All rights reserved.
//

import UIKit
import Firebase

class LoginVC: UIViewController {

  let logoContainerView: UIView = {
        let view = UIView()
        let logoImageView = UIImageView(image: #imageLiteral(resourceName: "Instagram_logo_white"))
        logoImageView.contentMode = .scaleAspectFill
    
        view.addSubview(logoImageView)
    
        logoImageView.anchor(top: nil, left: nil, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 200, height: 50)
        logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        logoImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    
        view.backgroundColor = UIColor(red: 0/255, green: 120/255, blue: 175/255, alpha: 1)
        return view
    }()
    
    let emailTextField : UITextField = {
       let tf = UITextField()
        
        tf.placeholder = "Email"
        tf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        tf.borderStyle = .roundedRect
        tf.font = .systemFont(ofSize: 14)
        tf.textColor = UIColor.black
        tf.addTarget(self, action: #selector(handleFormValidation), for: .editingChanged)
        
        return tf;
    }()
    
    let passwordTextField : UITextField = {
        
        let ptf = UITextField()
        ptf.placeholder = "Password"
        ptf.isSecureTextEntry = true
        ptf.backgroundColor = UIColor(white: 0, alpha: 0.03)
        ptf.borderStyle = .roundedRect
        ptf.textColor = UIColor.black
        ptf.font = .systemFont(ofSize: 14)
        ptf.addTarget(self, action: #selector(handleFormValidation), for: .editingChanged)
        return ptf
    }()
    
    let loginButton : UIButton = {
        
        let button = UIButton(type: .system)
        button.setTitle("Login", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor(red: 149/255, green: 204/255, blue: 244/255, alpha: 1)
        button.layer.cornerRadius = 5;
        button.isEnabled = false
        button.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        return button
    }()
    let dontHaveAccoubtButton : UIButton = {
       
        let button = UIButton(type: .system)
        
        let attributedTitle = NSMutableAttributedString(string: "Don't have an account?  ", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        attributedTitle.append(NSAttributedString(string: "Sign Up", attributes: [NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor: UIColor(displayP3Red: 17/255, green: 154/255, blue: 237/255, alpha: 1)]))
        
        button.setAttributedTitle(attributedTitle, for: .normal)
        button.addTarget(self, action: #selector(handleShowSignUp), for: .touchUpInside)
        return button
    }()
    //MARK: - View Did load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        navigationController?.navigationBar.isHidden = true
        
        view.addSubview(logoContainerView)
        
        logoContainerView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 150)
       
        
        view.addSubview(dontHaveAccoubtButton)
        dontHaveAccoubtButton.anchor(top: nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50)
        configureViewComponent()
    }
    
    //MARK: - Email Password put in stack view
    func configureViewComponent() {
        
        let stackView = UIStackView(arrangedSubviews: [emailTextField,passwordTextField,loginButton])
        
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.distribution = .fillEqually
        
        view.addSubview(stackView)
       
        stackView.anchor(top: logoContainerView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 40, paddingLeft: 20, paddingBottom: 0, paddingRight: 20, width: 0, height: 140)
    }
 
    @objc func handleShowSignUp(){
        let signUpVC = SignUpVC()
        navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    @objc func handleLogin(){
        guard let email = emailTextField.text,
              let password = passwordTextField.text else {
              return
        }
        
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            
            if let error = error {
                print("Error Sign In Process Pls Try Again \(error.localizedDescription)")
                return
            }
            
            print("Login Successful")
          // let keyWindow =  UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            guard let mainTabVC = UIApplication.shared.keyWindow?.rootViewController as? ManTabVC else {return}
            
            mainTabVC.configureViewControllers()
            self.dismiss(animated: true, completion: nil)
            

        }
        
    }
    
    @objc func handleFormValidation(){
        
        guard emailTextField.hasText,
            passwordTextField.hasText else {
                
                loginButton.isEnabled = false
                loginButton.backgroundColor = UIColor(red: 149/255, green: 204/255, blue: 244/255, alpha: 1)
                
                return
                
        }
        loginButton.isEnabled = true
        loginButton.backgroundColor = UIColor(red: 17/255, green: 154/255, blue: 237/255, alpha: 1)
        
    }
}
