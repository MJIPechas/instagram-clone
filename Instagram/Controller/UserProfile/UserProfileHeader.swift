//
//  UserProfileHeader.swift
//  Instagram
//
//  Created by Mohammad Jahir on 11/10/19.
//  Copyright © 2019 Mohammad Jahir. All rights reserved.
//

import UIKit

class UserProfileHeader: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .red
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
