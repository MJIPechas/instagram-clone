//
//  FeedVC.swift
//  Instagram
//
//  Created by Mohammad Jahir on 10/30/19.
//  Copyright © 2019 Mohammad Jahir. All rights reserved.
//

import UIKit
import Firebase

private let reuseIdentifier = "Cell"

class FeedVC: UICollectionViewController {

    
    //MARK: - Properties
    
    override func viewDidLoad() {
        super.viewDidLoad()

       

        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
        //Call Navigation button for logout
        configureLogOutButton()
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
    
        // Configure the cell
    
        return cell
    }


    //MARK: - Handlers
    func configureLogOutButton(){
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleLogOut))
    }
    
    @objc func handleLogOut(){
       
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        //add Logout action
        alertController.addAction(UIAlertAction(title: "Logout", style: .destructive, handler: { (_) in
            
            do{
              try  Auth.auth().signOut()

              let loginVC = LoginVC()
              let navControllerVC = UINavigationController(rootViewController: loginVC)
                navControllerVC.modalPresentationStyle = .fullScreen
              self.present(navControllerVC, animated: true, completion: nil)
                print("Successfully log user out")
            }catch{
                print("Failed to print out")
            }
            
            
            
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
        
    }
}
